
.. raw:: html

   <a rel="me" href="https://babka.social/@pvergain"></a>
   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://kolektiva.social/@noamsw"></a>


|FluxWeb| `RSS <https://judaism.gitlab.io/linkertree/rss.xml>`_


.. _linkertree_judaisme:
.. _links_judaisme:

=====================================================================
🕎 🕯️🌄🎶💐💗⚖  **Liens Judaïsme** 🕎🕯️ 🌄🎶💐💗⚖
=====================================================================

#ShalomSalaam #CeaseFireNow #Israel #Gaza #Mazeldon #Jewdiverse #Peace #Palestine
#jewish #ethics #morals #values #betterworld #israel #hebrew
#solidariteisrael #solidaritefrance #againstterorrism #culturejuive
#cultureenpartage #festival #centredartetdeculture

Mastowall
===========

- https://rstockm.github.io/mastowall/?hashtags=peace,paix,pace&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=mazeldon,jewish,jewdiverse&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=torah&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=chinsky&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=klezmer&server=https://framapiaf.org

Judaïsme
==============

- https://judaism.gitlab.io/judaisme
- https://judaism.gitlab.io/judaisme-2024
- https://judaism.gitlab.io/judaisme-2023/
- https://judaism.gitlab.io/judaisme-2020/
- https://judaism.gitlab.io/judaisme-2019/

Calendriers juifs
===================

- http://www.calj.net/
- https://www.hebcal.com/
- https://programme.yiddish.paris/?post_type=tribe_events&eventDisplay=month

Rabba Floriane Chinsky
========================

Site rabbinchinsky.fr
-----------------------

- https://rabbinchinsky.fr
- https://judaism.gitlab.io/floriane_chinsky/
- https://judaism.gitlab.io/bokertov/

Shabbats
++++++++++++

- https://judaism.gitlab.io/shabbats/


Youtube
-----------

- https://www.youtube.com/c/FlorianeChinsky/videos
- https://invidious.fdn.fr/channel/UCDtvtoXqGz0Niwl2QGM8gyQ

Peertube
------------

- https://peertube.iriseden.eu/a/floriane_c/video-channels


Bokertov
-------------

- https://judaism.gitlab.io/bokertov/

American friends of the parents circle
===========================================

- https://parentscirclefriends.org/
- https://linktr.ee/afpcff
- https://www.instagram.com/parentscirclefriends/


**Antiracisme**
==========================================================================

- https://antiracisme.frame.io/linkertree
- https://antiracisme.frama.io/infos-2024
- https://antiracisme.frama.io/infos-2023
- https://antiracisme.frama.io/infos (2021-2022)

Women of the Wall
===================

- https://womenofthewall.org.il/
- https://www.instagram.com/womenofthewall/


Guerrières de la paix
=========================

- https://luttes.frama.io/pour/la-paix/les-guerrieres-de-la-paix/

Women Wage Peace (WWP)
===========================

- https://luttes.frama.io/pour/la-paix/womenwagepeace/


Akadem TV
============

- https://www.youtube.com/@AkademTV/videos

Mahj
=====

- https://www.mahj.org/fr


Institut Maïmonide
====================

- https://maimonide-institut.com/ devient http://iumat.fr/
- http://iumat.fr/
- https://www.youtube.com/@michaeliancu-institutmaimo1303/videos
- https://maimonide-institut.com/sites/default/files/plaquette_maimonide_2023-2024.pdf


IEMJ (Institut Européen des Musiques Juives)
===============================================

- https://www.iemj.org/

Tenoua
==========

- https://www.tenoua.org/
- https://www.instagram.com/tenoua/

Pourim Shpil
================

- https://www.pourimshpil.eu/
- https://www.pourimshpil.eu/qui-sommes-nous/


Culture Juive
==============

- https://www.culture-juive.fr


Adama
========

- https://adamah.org/

btselem
===========

- https://www.btselem.org/
- https://www.youtube.com/@btselem/videos



Cercle Bernard Lazare
========================

- https://www.cerclebernardlazare.org/
- https://cbl.frama.io/cbl-grenoble/linkertree/
- https://cbl.frama.io/cbl-grenoble/decennie-2020
- https://cbl.frama.io/cbl-grenoble/infos


Histoire
===========

- https://www.odyeda.com/fr/


Talmudiques
=============

- https://www.radiofrance.fr/franceculture/podcasts/talmudiques

UEJF
=====

- https://uejf.org/
- https://nitter.cz/uejf/rss

Centre Medem
===============

- https://www.centre-medem.org/


A l'origine
================

- https://www.france.tv/france-2/a-l-origine/toutes-les-videos/

K la revue
===========

- https://k-larevue.com/

Oyeda
========

- http://www.odyeda.com/fr/

Hazon
=======

- https://hazon.org/

Yiddish
==========

- https://www.yiddishweb.com/
- https://www.instagram.com/parisyiddish/

programme.yiddish.paris
===========================

- https://programme.yiddish.paris/

Journaux
============

- https://www.972mag.com/
- https://fr.timesofisrael.com/
- https://www.haaretz.com/

Contre l'antisémitisme
============================

- https://luttes.frama.io/contre/l-antisemitisme/
- https://antiracisme.frama.io/linkertree
